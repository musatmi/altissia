package org.altissia.recruitment.interview.businesslogic.rules.impl;

import org.altissia.recruitment.interview.businesslogic.rules.Rule;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Order(0)
@Component
public class ZeroRule implements Rule {
    @Override
    public Optional<String> evaluate(int number) {

        if (number == 0) {
            return Optional.of("0");
        }
        return Optional.empty();
    }
}
