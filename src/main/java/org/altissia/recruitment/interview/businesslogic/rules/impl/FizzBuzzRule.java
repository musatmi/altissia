package org.altissia.recruitment.interview.businesslogic.rules.impl;

import org.altissia.recruitment.interview.businesslogic.rules.Rule;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Order(2)
@Component
public class FizzBuzzRule implements Rule {

    @Override
    public Optional<String> evaluate(int number) {

        if (number % 3 == 0 && number % 5 == 0) {
            return Optional.of("fizzbuzz");
        }

        if (number % 3 == 0) {
            return Optional.of("fizz");
        }

        if (number % 5 == 0) {
            return Optional.of("buzz");
        }

        return Optional.empty();
    }
}
