package org.altissia.recruitment.interview.businesslogic.rules.impl;

import org.altissia.recruitment.interview.businesslogic.rules.Rule;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Order(1)
@Component
public class FizzBuzzBazzRule implements Rule {
    @Override
    public Optional<String> evaluate(int number) {

        if (number % 3 == 0 && number % 5 == 0 && number % 7 == 0) {
            return Optional.of("FizzBuzzBazz");
        }

        if (number % 5 == 0 && number % 7 == 0) {
            return Optional.of("BuzzBazz");
        }

        if (number % 3 == 0 && number % 7 == 0) {
            return Optional.of("FizzBazz");
        }

        if (number % 7 == 0) {
            return Optional.of("Bazz");
        }

        return Optional.empty();
    }
}
