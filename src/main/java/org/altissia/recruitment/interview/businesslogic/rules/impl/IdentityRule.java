package org.altissia.recruitment.interview.businesslogic.rules.impl;

import org.altissia.recruitment.interview.businesslogic.rules.Rule;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Order(3)
@Component
public class IdentityRule implements Rule {
    @Override
    public Optional<String> evaluate(int number) {
        return Optional.of(Integer.toString(number));
    }
}
