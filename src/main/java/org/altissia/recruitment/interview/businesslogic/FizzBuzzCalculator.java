package org.altissia.recruitment.interview.businesslogic;

import org.altissia.recruitment.interview.businesslogic.rules.Rule;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class FizzBuzzCalculator {

    public final static int DEFAULT_RANGE_START = 1;
    public final static int DEFAULT_RANGE_END = 100;

    private final List<Rule> rules;

    public FizzBuzzCalculator(List<Rule> rules) {
        this.rules = rules;
    }

    public String fizzBuzz(int number) {

        for (Rule rule : rules) {
            Optional<String> evaluation = rule.evaluate(number);
            if (evaluation.isPresent()) {
                return evaluation.get();
            }
        }

        throw new RuntimeException("No rule matched");
    }

    public List<String> fizzBuzz(int start, int end) {

        List<String> results = new ArrayList<>();

        for (int i = start; i <= end; i++) {
            results.add(fizzBuzz(i));
        }
        return results;
    }
}
