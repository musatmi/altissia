package org.altissia.recruitment.interview.businesslogic.rules;

import java.util.Optional;

public interface Rule {

    Optional<String> evaluate(int number);
}
