package org.altissia.recruitment.interview.rest;

import org.altissia.recruitment.interview.businesslogic.FizzBuzzCalculator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.altissia.recruitment.interview.businesslogic.FizzBuzzCalculator.DEFAULT_RANGE_END;
import static org.altissia.recruitment.interview.businesslogic.FizzBuzzCalculator.DEFAULT_RANGE_START;

@RestController
@RequestMapping(path = "/fizzbuzz")
public class FizzBuzzController {

    private final FizzBuzzCalculator calculator;

    public FizzBuzzController(FizzBuzzCalculator calculator) {
        this.calculator = calculator;
    }

    @GetMapping
    public List<String> fizzBuzz(@RequestParam Optional<List<String>> entry) {

        List<String> output;

        if (entry.isEmpty() || entry.get() == null || entry.get().size() == 0) {
            output = this.calculator.fizzBuzz(DEFAULT_RANGE_START, DEFAULT_RANGE_END);
        } else {
            output = entry.get().stream().map(Integer::parseInt).map(this.calculator::fizzBuzz).collect(Collectors.toList());
        }

        return output;
    }
}


