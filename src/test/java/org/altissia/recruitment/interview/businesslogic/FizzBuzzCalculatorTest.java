package org.altissia.recruitment.interview.businesslogic;

import org.altissia.recruitment.interview.businesslogic.rules.impl.FizzBuzzBazzRule;
import org.altissia.recruitment.interview.businesslogic.rules.impl.FizzBuzzRule;
import org.altissia.recruitment.interview.businesslogic.rules.impl.IdentityRule;
import org.altissia.recruitment.interview.businesslogic.rules.impl.ZeroRule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FizzBuzzCalculatorTest {

    private static FizzBuzzCalculator fizzBuzzCalculator;

    @BeforeAll
    static void setUp() {
        fizzBuzzCalculator = new FizzBuzzCalculator(Arrays.asList(new ZeroRule(), new FizzBuzzBazzRule(), new FizzBuzzRule(), new IdentityRule()));
    }


    @Test
    void fizzBuzz_single_param() {
        assertEquals("0", fizzBuzzCalculator.fizzBuzz(0));
        assertEquals("1", fizzBuzzCalculator.fizzBuzz(1));
        assertEquals("fizz", fizzBuzzCalculator.fizzBuzz(3));
        assertEquals("buzz", fizzBuzzCalculator.fizzBuzz(5));
        assertEquals("Bazz", fizzBuzzCalculator.fizzBuzz(7));
        assertEquals("fizzbuzz", fizzBuzzCalculator.fizzBuzz(15));
        assertEquals("FizzBazz", fizzBuzzCalculator.fizzBuzz(21));
        assertEquals("BuzzBazz", fizzBuzzCalculator.fizzBuzz(70));
        assertEquals("FizzBuzzBazz", fizzBuzzCalculator.fizzBuzz(315));
        assertEquals("-1", fizzBuzzCalculator.fizzBuzz(-1));
    }

    @Test
    void fizzBuzz_range() {
        var expected = Arrays.asList("fizz", "-2", "-1", "0", "1", "2", "fizz", "4", "buzz", "fizz", "Bazz", "8",
                "fizz", "buzz", "11", "fizz", "13", "Bazz", "fizzbuzz", "16");
        assertEquals(expected, fizzBuzzCalculator.fizzBuzz(-3, 16));
    }
}