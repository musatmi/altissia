package org.altissia.recruitment.interview;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.altissia.recruitment.interview.businesslogic.FizzBuzzCalculator.DEFAULT_RANGE_END;
import static org.altissia.recruitment.interview.businesslogic.FizzBuzzCalculator.DEFAULT_RANGE_START;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class InterviewApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;
    private final ParameterizedTypeReference<List<String>> listResponseType = new ParameterizedTypeReference<>() {
    };

    @LocalServerPort
    private int port;


    @Test
    void test_contextLoads() {
    }

    @Test
    void test_happyFlow() {
        ResponseEntity<List<String>> response = this.restTemplate.exchange("http://localhost:" + port + "/fizzbuzz?entry=3", HttpMethod.GET, null, listResponseType);
        assertEquals(response.getBody().get(0), "fizz");
    }

    @Test
    void test_negative() {
        ResponseEntity<List<String>> response = this.restTemplate.exchange("http://localhost:" + port + "/fizzbuzz?entry=-1", HttpMethod.GET, null, listResponseType);
        assertEquals(response.getBody().get(0), "-1");
    }

    @Test
    void test_entryBlank() {
        ResponseEntity<List<String>> response = this.restTemplate.exchange("http://localhost:" + port + "/fizzbuzz?entry=", HttpMethod.GET, null, listResponseType);
        assertEquals(response.getBody().size(), DEFAULT_RANGE_END - DEFAULT_RANGE_START + 1);
    }

    @Test
    void test_arrayInput() {
        ResponseEntity<List<String>> response = this.restTemplate.exchange("http://localhost:" + port + "/fizzbuzz?entry=3,7,15,343,1097", HttpMethod.GET, null, listResponseType);
        assertEquals(response.getBody().size(), 5);
        assertEquals(response.getBody(), List.of("fizz", "Bazz", "fizzbuzz", "Bazz", "1097"));
    }

    @Test
    void test_entryNotAnInt() {
        ResponseEntity<Object> response = this.restTemplate.getForEntity("http://localhost:" + port + "/fizzbuzz?entry=hg54,23,343", Object.class);
        assertEquals(response.getStatusCode(), INTERNAL_SERVER_ERROR);
    }


}
