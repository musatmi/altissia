package org.altissia.recruitment.interview.rest;

import org.altissia.recruitment.interview.businesslogic.FizzBuzzCalculator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(FizzBuzzController.class)
class FizzBuzzControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FizzBuzzCalculator calculator;


    @BeforeEach
    void setUp() {
        when(calculator.fizzBuzz(any(Integer.class))).thenReturn("bogusString");
        when(calculator.fizzBuzz(any(Integer.class), any(Integer.class))).thenReturn(Arrays.asList("bogusString1", "bogusString2"));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void fizzBuzz_happy_flow() throws Exception {
        mvc.perform(get("/fizzbuzz?entry=3")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0]", is("bogusString")));


    }

    @Test
    void fizzBuzz_entry_blank() throws Exception {
        mvc.perform(get("/fizzbuzz?entry=")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0]", is("bogusString1")))
                .andExpect(jsonPath("$[1]", is("bogusString2")));

    }
}